# alpine-mariadb

a docker image base on alpine with mariadb
(based on [wangxian/alpine-mysql](https://github.com/wangxian/alpine-mysql.git) )

# build image

```
docker build -t yujiorama/alpine-mariadb .
```

# Usage

same as [Docker Official Image MySQL](https://github.com/docker-library/mysql)

# Stats

```
$ docker images
REPOSITORY                 TAG                 IMAGE ID            CREATED             SIZE
yujiorama/alpine-mariadb   latest              8f197e3650b2        16 seconds ago      174.2 MB
mysql                      5.7                 4b3b6b994512        2 weeks ago         384.5 MB
```
