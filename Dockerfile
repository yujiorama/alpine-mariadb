FROM alpine:latest
MAINTAINER yujiorama <yujiorama@gmail.com>

ENV GOSU_VERSION 1.8
RUN set -x \
 && apk --no-cache --update --virtual=.gosu-deps add curl gnupg openssl \
  && curl -o /usr/local/bin/gosu -sL "https://github.com/tianon/gosu/releases/download/${GOSU_VERSION}/gosu-amd64" \
  && curl -o /usr/local/bin/gosu.asc -sL "https://github.com/tianon/gosu/releases/download/${GOSU_VERSION}/gosu-amd64.asc" \
  && export GNUPGHOME="$(mktemp -d)" \
  && gpg --keyserver ha.pool.sks-keyservers.net --recv-keys B42F6819007F00F88E364FD4036A9C25BF357DD4 \
  && gpg --batch --verify /usr/local/bin/gosu.asc /usr/local/bin/gosu \
  && rm -r "$GNUPGHOME" /usr/local/bin/gosu.asc \
  && chmod +x /usr/local/bin/gosu \
  && gosu nobody true \
  && apk --purge -v del .gosu-deps \
 && apk --no-cache --update add bash tar zip tzdata pwgen mariadb mariadb-client \
  && cp /usr/share/zoneinfo/Asia/Tokyo /etc/localtime \
  && gosu mysql true \
  && rm -rf /var/lib/mysql \
  && mkdir -p /etc/mysql/conf.d /etc/mysql/mysql.conf.d /var/lib/mysql /var/run/mysqld /docker-entrypoint-initdb.d \
  && chown -R mysql:mysql /etc/mysql/conf.d /etc/mysql/mysql.conf.d /var/lib/mysql /var/run/mysqld \
  && chmod 777 /var/run/mysqld \
 && rm /bin/sh && ln -s /bin/bash /bin/sh \
 && rm -rf /var/cache/apk/*

VOLUME /var/lib/mysql

COPY my.cnf /etc/mysql/my.cnf
COPY mysql.cnf /etc/mysql/mysql.conf.d/mysql.cnf
COPY docker-entrypoint.sh /usr/local/bin/
RUN chmod 755 /usr/local/bin/docker-entrypoint.sh \
 && chmod 644 /etc/mysql/my.cnf /etc/mysql/mysql.conf.d/mysql.cnf

ENTRYPOINT ["docker-entrypoint.sh"]

EXPOSE 3306
CMD ["mysqld"]
